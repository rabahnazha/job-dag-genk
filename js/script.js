$(document).ready(function () {
  //change menu bars to cross
  $('.navbar-toggler').click(function () {
    $('.navbar-toggler').toggleClass('change');
  });

  //sticky navbar and less padding
  $(window).scroll(function () {
    let position = $(this).scrollTop();

    //if navbar reaches the top ==> 2 css classes added
    if (position >= 718) {
      $('.navbar').addClass('navbar-background');
      $('.navbar').addClass('fixed-top');
    } else {
      //if position is < 718
      $('.navbar').removeClass('navbar-background');
      $('.navbar').removeClass('fixed-top');
    }
  });

  // Add smooth scrolling to all links
  // $('a').on('click', function (event) {
  //   // Make sure this.hash has a value before overriding default behavior
  //   if (this.hash !== '') {
  //     // Prevent default anchor click behavior
  //     event.preventDefault();

  //     // Store hash
  //     var hash = this.hash;

  //     // Using jQuery's animate() method to add smooth page scroll
  //     // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
  //     $('html, body').animate(
  //       {
  //         scrollTop: $(hash).offset().top - 20,
  //       },
  //       2000,
  //       function () {
  //         // Add hash (#) to URL when done scrolling (default click behavior)
  //         window.location.hash = hash;
  //       }
  //     );
  //   } // End if
  // });


  ////////////// skills items Nazha/////////////////
  $('.gallery-link').on('click', function () {
    $(this).find('.gallery').magnificPopup('open');
  });

  $('.gallery').each(function () {
    $(this).magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true,
      },
    });
  });
});
/////////////// 2nd Choice Carousel Course Items JavaScript //////////////
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides((slideIndex += n));
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides((slideIndex = n));
}
function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName('mySlides');
  var dots = document.getElementsByClassName('dot');
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = 'none';
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(' active', '');
  }
  slides[slideIndex - 1].style.display = 'block';
  dots[slideIndex - 1].className += ' active';
}

////////// Count down ////////////////////////

let countDownDate = new Date('Jun 15, 2021 00:00:00').getTime();

// Updating the count down every 1 second
let x = setInterval(function () {
  // Geting today's date & time
  let now = new Date().getTime();

  // Finding distance between now and the count down date
  let distance = countDownDate - now;

  // Calculating for days, hours, minutes and seconds
  let days = Math.floor(distance / (1000 * 60 * 60 * 24));
  let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((distance % (1000 * 60)) / 1000);

  document.getElementById('jobday-countdown').innerHTML =
    days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's ';

  if (distance < 0) {
    clearInterval(x);
    document.getElementById('jobday-countdown').innerHTML = 'EXPIRED';
  }
}, 1000);

const javaDeveloper = new AutoTyping({
  id: 'java-developer',
  typeText: ['Find your Java Developer'],
  textColor: '#fff',
}).init();

const itEmpIntro = new AutoTyping({
  id: 'it-emp-intro',
  typeText: ['A digital introduction to potential IT employees'],
  textColor: '#fff',
}).init();

